import sendgrid
from django.core.mail import EmailMultiAlternatives
from django.conf import settings
from django.template.loader import render_to_string
from django.template import RequestContext


class SendEmail(object):

    def __init__(self, request=None, headers=None, sender=None, from_name=None, backend=None , file=[]):
        self.request = request
        self.headers = headers
        self.file = file

        if from_name:
            self.from_name = from_name
        else:
            self.from_name = settings.DEFAULT_FROM_EMAIL_NAME

        if sender:
            self.sender = sender
        else:
            self.sender = settings.DEFAULT_FROM_EMAIL

    def send(self, recipient, template_path, context, subject, bcc_email=[]):
        """
        send email function with template_path. will do both rendering & send in this function.
        should not change the interface.
        """
        
        body = self.email_render(template_path, context)
        self.send_email(recipient, subject, body, bcc_email)
        

    def send_email(self, recipient, subject, body, bcc_email):
        """
        send email with rendered subject and body
        """
        '''if bcc_email:
            msg = EmailMultiAlternatives(subject, subject, self.sender, recipient, bcc=bcc_email)
        else:
            msg = EmailMultiAlternatives(subject, subject, self.sender, recipient)

        msg.attach_alternative(body, "text/html")

        if self.file:
            for file in self.file:
                msg.attach_file(file)
        msg.send()'''
        recipient = [str(user) for user in recipient]
        sg = sendgrid.SendGridClient(settings.SENDGRID_API_KEY)
        message = sendgrid.Mail()
        message.smtpapi.add_to(recipient)
        message.set_subject(subject)
        message.set_html(body)
        message.set_text(subject)
        message.set_from(settings.DEFAULT_FROM_EMAIL)

        if self.file:
            for file in self.file:
                file_name = file.split('/')[-1]
                message.add_attachment(file_name, str(file))

        sg.send(message)

    def email_render(self, template_path, context):
        """
        wrapper to generate email subject and body
        """
        if self.request is None:
            body = render_to_string(template_path, context)
        else:
            body = render_to_string(template_path, context, RequestContext(self.request))
        return body
