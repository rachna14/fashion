from django.contrib import admin

from .models import Buyer, RecentlyViewed, Feedback, CartItems, Order, OrderItem, DesignRequest


class BuyerAdmin(admin.ModelAdmin):
    list_display = ['user', 'phone_no', 'city', 'state', 'country']
    search_fields = ['user__first_name', 'user__last_name']


class RecentlyViewedAdmin(admin.ModelAdmin):
    list_display = ['user', 'design']
    search_fields = ['user']


class FeedbackAdmin(admin.ModelAdmin):
    list_display = ['buyer', 'design', 'rating', 'created']
    search_fields = ['buyer', 'design']


class CartItemsAdmin(admin.ModelAdmin):
    list_display = ['buyer', 'design', 'size']
    search_fields = ['design']


class OrderAdmin(admin.ModelAdmin):
    list_display = ['buyer', 'total_price', 'charge_id', 'payement_method', 'order_status']
    search_fields = ['buyer']


class OrderItemAdmin(admin.ModelAdmin):
    list_display = ['order', 'design', 'size', 'unit_price', 'order_item_status']
    search_fields = ['design', 'order']


class DesignRequestAdmin(admin.ModelAdmin):
    list_display = ['title', 'buyer', 'designer', 'request_status']
    search_fields = ['title']


admin.site.register(Buyer, BuyerAdmin)
admin.site.register(RecentlyViewed, RecentlyViewedAdmin)
admin.site.register(Feedback, FeedbackAdmin)
admin.site.register(CartItems, CartItemsAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(OrderItem, OrderItemAdmin)
admin.site.register(DesignRequest, DesignRequestAdmin)