from django.conf.urls import url


urlpatterns = [
    url(r'^registration-designer/$', 'designer.views.registration', name='registration'),
    url(r'^(?P<username>\w+)/$', 'designer.views.profile', name='profile'),
    url(r'^edit-profile/$', 'designer.views.edit_designer', name='edit_designer'),
    url(r'^upload-design/$', 'designer.views.upload_design', name='upload_design'),
    url(r'^my-designs/$', 'designer.views.my_designs', name='my_designs'),
    url(r'^design-details/(?P<design_id>[0-9]+)/$', 'designer.views.design_details', name='design_details'),
    url(r'^delete-designs/$', 'designer.views.delete_design', name='delete_design'),
    url(r'^edit-designs/(?P<design_id>[0-9]+)/$', 'designer.views.edit_design', name='edit_design'),
    url(r'^get-dress-types/$', 'designer.views.get_dress_type', name='get_dress_type'),
    url(r'^design-request/$', 'designer.views.design_requests_designer', name='design_requests_designer'),
    url(r'^accept-reject-request/$', 'designer.views.accept_reject_request', name='accept_reject_request'),
]
