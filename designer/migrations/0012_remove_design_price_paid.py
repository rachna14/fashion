# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-03-12 12:51
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('designer', '0011_auto_20170312_1820'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='design',
            name='price_paid',
        ),
    ]
